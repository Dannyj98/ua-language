package uk.ac.ljmu.fet.cs.comp.uax;


import java.io.PrintWriter;
import uk.ac.ljmu.fet.cs.comp.interpreter.ParseUA;
import uk.ac.ljmu.fet.cs.comp.interpreter.UAMachine;
import uk.ac.ljmu.fet.cs.comp.interpreter.tokens.Expression;

public class UAXCompile {

	private static final String FILENAME = "sample.ua";
	
	public static void main(String[] args) {

		try {
			ParseUA.load(args[0]);
		} catch (Throwable e) {
			System.err.println("Could not parse the file " + args[0]);
			System.err.println(e.getMessage());
			System.exit(1);
		}
		
		try {
			PrintWriter write = new PrintWriter(FILENAME);
			
			for (Expression exp : UAMachine.theProgram) {
				System.out.println(exp.toOriginalUA());
				write.println(exp.toOriginalUA());
				
			}
			write.close();
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
